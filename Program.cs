using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            int total = program.GetTotalTax(5, 32, 1000);
            Console.WriteLine(total);

            string congratulation = program.GetCongratulation(22);
            congratulation += '\n' + program.GetCongratulation(21);
            congratulation += '\n' + program.GetCongratulation(15);
            Console.WriteLine(congratulation);

            double product = program.GetMultipliedNumbers("2", "3");
            Console.WriteLine(product);
            product = program.GetMultipliedNumbers("2.6", "2,0");
            Console.WriteLine(product);

            Dimensions dimensions = new Dimensions();
            dimensions.FirstSide = 1;
            dimensions.SecondSide = 2;
            dimensions.ThirdSide = 3;
            dimensions.Radius = 4;
            dimensions.Diameter = 8;
            dimensions.Height = 4;

            double square = program.GetFigureValues(Figure.Circle, Parameter.Square, dimensions);
            Console.WriteLine(square);
            double perimeter = program.GetFigureValues(Figure.Circle, Parameter.Perimeter, dimensions);
            Console.WriteLine(perimeter);

            square = program.GetFigureValues(Figure.Rectangle, Parameter.Square, dimensions);
            Console.WriteLine(square);
            perimeter = program.GetFigureValues(Figure.Rectangle, Parameter.Perimeter, dimensions);
            Console.WriteLine(perimeter);

            square = program.GetFigureValues(Figure.Triangle, Parameter.Square, dimensions);
            Console.WriteLine(square);
            perimeter = program.GetFigureValues(Figure.Triangle, Parameter.Perimeter, dimensions);
            Console.WriteLine(perimeter);

        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companiesNumber * tax * companyRevenue / 100;
        }

        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18)
            {
                return "���������� � ����������������!";
            }
            if (input % 2 != 0 && (input < 18 && input > 12))
            {
                return "���������� � ��������� � ������� �����!";
            }
            else
            {
                return $"���������� � {input}-������!";
            }
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            return double.Parse(first.Replace('.', ',')) * double.Parse(second.Replace('.', ','));
        }

        public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            if (figureType == Figure.Circle && parameterToCompute == Parameter.Perimeter)
            {
                return dimensions.Diameter * Math.PI;
            }
            if (figureType == Figure.Circle && parameterToCompute == Parameter.Square)
            {
                return Math.PI * Math.Pow(dimensions.Radius, 2);
            }
            if (figureType == Figure.Rectangle && parameterToCompute == Parameter.Perimeter)
            {
                return 2 * (dimensions.FirstSide + dimensions.SecondSide);
            }
            if (figureType == Figure.Rectangle && parameterToCompute == Parameter.Square)
            {
                return dimensions.FirstSide * dimensions.SecondSide;
            }
            if (figureType == Figure.Triangle && parameterToCompute == Parameter.Perimeter)
            {
                return dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
            }
            if (figureType == Figure.Triangle && parameterToCompute == Parameter.Square)
            {
                return 0.5 * dimensions.FirstSide * dimensions.Height;
            }
            return double.NaN;
        }
    }
}
